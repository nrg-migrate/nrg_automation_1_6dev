package org.nrg.automation.runners;

import org.nrg.automation.annotations.Supports;

@Supports("python")
public class PythonScriptRunner extends AbstractScriptRunner {
}
