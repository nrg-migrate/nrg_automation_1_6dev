package org.nrg.automation.runners;

import org.nrg.automation.annotations.Supports;

@Supports("groovy")
public class GroovyScriptRunner extends AbstractScriptRunner {
}
